# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-rc.0] - 2021-06-19
#### Under the hood changes
- Initiated project with vue cli (Vue 3 (TypeScript), Vue-Router, Vuex, SCSS)
- Setup linter configs (Eslint, Stylelint)
- Added i18n for localization
- Added branding CSS variables, logo and favicon
- Added no script html block
- Nprogress to display progress bar during loading routes
- Added Release sanity check for check release version
- Project configs

[Unreleased]: https://gitlab.com/napalmpapalam/nasa-search/compare/0.1.0-rc.0...main
[0.1.0-rc.0]: https://gitlab.com/napalmpapalam/nasa-search/tags/0.1.0-rc.0
