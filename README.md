# What is it?

It's a Vue3 + TS bundle practice.

The main goal is to practice with the typescript and setup of the project, also
using [NASA Image and Video Library](https://images.nasa.gov/docs/images.nasa.gov_api_docs.pdf)
for building simple search application like [this](https://images.nasa.gov/) 

## Project setup and run
```
yarn
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Lints project with ESlint and Stylelint
```
yarn lint
```

### Lints release version
``` 
yarn rsc %version%
``` 
Or it will do automatically before pushing tag into the ```main``` branch using
version in the ```package.json``` and ```CHANGELOG.md```

### Webpack analyze bundle tool
```
yarn analyze-bundle
```
