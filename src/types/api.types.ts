import type { AxiosResponse, Method } from 'axios'

import {
  BadRequestError,
  NotFoundError,
  InternalServerError,
  ServerError,
} from '@/api/errors'

export type { 
  Method,
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse, 
} from 'axios'

export type RequestResponse = AxiosResponse

export interface RequestMethods { [key: string]: Method }

export interface RequestData { [key: string]: any }

export interface RequestParams { [key: string]: any }

export interface RequestConfig {
  endpoint: string,
  method: Method,
  query?: RequestParams,
  data?: RequestData,
  isEmptyBodyAllowed?: boolean,
  contentType?: string
}

export interface StatusCodes { [key: string]: number }

export type ApiError =
  | BadRequestError
  | NotFoundError
  | InternalServerError
  | ServerError