import packageJson from '../package.json'

interface Config {
  APP_NAME: string,
  STORAGE_KEY: string
  BUILD_VERSION: string
}

export const CONFIG: Config = Object.freeze({
  APP_NAME: 'NASA Library Search',

  STORAGE_KEY: 'nasa-search-storage',

  BUILD_VERSION: packageJson.version,
})
