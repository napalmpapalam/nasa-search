import { createI18n } from 'vue-i18n'
import resources from './resources'

const STORAGE_KEY = 'locale'
const DEFAULT_LOCALE = 'en'

const locale = localStorage && localStorage.getItem(STORAGE_KEY)
  ? localStorage.getItem(STORAGE_KEY) || undefined
  : DEFAULT_LOCALE

const i18n = createI18n({
  locale,
  fallbackLocale: locale,
  silentFallbackWarn: true,
  messages: { ...resources },
})

export { i18n }
