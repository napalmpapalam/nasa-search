import { createStore } from 'vuex'

export const store = createStore({})

export { vuexTypes } from './types'
