import AppContent from '@/AppContent.vue'
import Search from '@/vue/pages/Search.vue'

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { vueRoutes } from '@/vue-router/routes'

import NProgress from 'nprogress'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/:catchAll(.*)',
    redirect: vueRoutes.app,
  },
  {
    path: '/',
    name: vueRoutes.app.name,
    redirect: vueRoutes.search,
    component: AppContent,
    children: [
      {
        path: '/search',
        name: vueRoutes.search.name,
        component: Search,
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior: () => ({ top: 0, left: 0, behavior: 'auto' }),
})

router.beforeEach(async (to, from, next) => { 
  if (to.name !== from.name) NProgress.start()
  next()
})

NProgress.configure({ showSpinner: false })

router.afterEach(() => { NProgress.done() })

export default router
export { vueRoutes }