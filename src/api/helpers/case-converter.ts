import _ from 'lodash'
import { AxiosResponse } from '@/types'

type LodashTransformKeyCaseFn = (param: AxiosResponse | string) => AxiosResponse

export function toCamelCaseDeep (object: AxiosResponse): AxiosResponse {
  return convertCaseDeep(object, _.camelCase)
}

export function toSnakeCaseDeep (object: AxiosResponse): AxiosResponse {
  return convertCaseDeep(object, _.snakeCase)
}

function convertCaseDeep (
  object: AxiosResponse,
  transformPropertyName: LodashTransformKeyCaseFn,
): AxiosResponse {
  if (_.isString(object) || _.isNumber(object)) { return object }
  if (_.isArray(object)) {
    return _.map(
      object, 
      (obj: AxiosResponse) => convertCaseDeep(obj, transformPropertyName),
    )
  }

  let convertedObject = _.cloneDeep(object)

  // Convert keys to camel case
  convertedObject = _.mapKeys(
    convertedObject,
    (value: any, key: string) => transformPropertyName(key),
  )

  // Recursively apply throughout object
  return _.mapValues(
    convertedObject,
    (value: any) => {
      if (_.isPlainObject(value)) {
        return convertCaseDeep(value, transformPropertyName)
      } else if (_.isArray(value)) {
        return _.map(
          (value: any, obj: AxiosResponse) => 
            convertCaseDeep(obj, transformPropertyName),
        )
      }
      return value
    },
  )
}
