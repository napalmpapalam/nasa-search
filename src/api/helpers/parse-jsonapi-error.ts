import * as errors from '../errors'
import type { StatusCodes, AxiosError, ApiError } from '@/types'

const STATUS_CODES: StatusCodes = {
  badRequest: 400,
  notFound: 404,
  internalError: 500,
}

export function parseJsonapiError (originalError: AxiosError): ApiError {
  let error
  const statusCode = originalError?.response?.status

  switch (statusCode) {
  case STATUS_CODES.badRequest:
    error = new errors.BadRequestError(originalError)
    break
  case STATUS_CODES.notFound:
    error = new errors.NotFoundError(originalError)
    break
  case STATUS_CODES.internalError:
    error = new errors.InternalServerError(originalError)
    break
  default:
    error = new errors.ServerError(originalError)
  }

  return error
}
