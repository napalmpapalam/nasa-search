import axios from 'axios';
import { parseJsonapiError } from './helpers/parse-jsonapi-error'
import { toCamelCaseDeep } from './helpers/case-converter'

import type { 
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  RequestConfig,
  RequestResponse,
  RequestMethods,
  RequestParams,
} from '@/types'
//   baseURL: 'https://images-api.nasa.gov/',

const REQUEST_METHODS: RequestMethods = Object.freeze({
  PATCH: 'PATCH',
  POST: 'POST',
  PUT: 'PUT',
  GET: 'GET',
  DELETE: 'DELETE',
})

export class Api {
  #axios: AxiosInstance
  #baseURL: string

  constructor(opts: { baseURL: string }) {
    this.#axios = axios.create()
    this.#baseURL = opts.baseURL
  }

  async #call (opts: RequestConfig): Promise<RequestResponse> {

    const config: AxiosRequestConfig = {
      baseURL: this.#baseURL,
      params: opts.query || {},
      data: (opts.isEmptyBodyAllowed && !opts.data)
        ? undefined
        : opts.data || {},
      method: opts.method,
      headers: {
        ['Content-Type']: 'application/vnd.api+json',
        ['Accept']: 'application/vnd.api+json',
      },
      url: opts.endpoint,
      maxContentLength: 100000000000,
      maxBodyLength: 1000000000000,
    }

    if (opts.contentType) { config.headers['Content-Type'] = opts.contentType }

    let response

    try {
      response = await this.#axios(config)
    } catch (e) {
      throw parseJsonapiError(e)
    }
    response = toCamelCaseDeep(response)

    return response
  }

  get (endpoint: string, query: RequestParams): Promise<AxiosResponse> {
    const config: RequestConfig = {
      method: REQUEST_METHODS.GET,
      endpoint,
      query,
      isEmptyBodyAllowed: true,
    }
    return this.#call(config)
  }
}
