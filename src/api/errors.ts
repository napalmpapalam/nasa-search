import { AxiosError } from '@/types'

export class ServerErrorBase extends Error {
  originalError: AxiosError

  constructor(originalError: AxiosError) {
    super(originalError.message)

    this.originalError = originalError
  }

  get httpStatus (): string | number | undefined {
    return this.originalError?.response?.status
  }
  
  get requestPath (): string | undefined {
    return this.originalError?.response?.request?.path
  }

  get reason (): string | undefined {
    return this.originalError?.response?.data?.reason
  }
}

export class BadRequestError extends ServerErrorBase {}

export class NotFoundError extends ServerErrorBase {}

export class InternalServerError extends ServerErrorBase {}

export class ServerError extends ServerErrorBase {}
