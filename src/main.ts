import '@scss/app.scss'

import App from '@/App.vue'
import router from '@/vue-router'

import { createApp } from 'vue'
import { store } from '@/vuex'
import { CONFIG } from '@/config'
import { i18n } from '@/i18n'
import { vueRoutes } from '@/vue-router/routes'

const app = createApp(App)

app.use(store).use(router).use(i18n)

app.config.globalProperties.$routes = vueRoutes
app.config.globalProperties.$config = CONFIG

app.mount('#app')